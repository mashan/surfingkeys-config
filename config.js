// refs. https://github.com/hushin/dotfiles/blob/master/docs/SurfingkeysSetting.js
// ---- Settings ----
api.Hints.characters = 'hlasdfgyuiowertnm'
settings.scrollStepSize = 140
settings.hintAlign = 'left'
settings.aceKeybindings = 'emacs'
settings.nextLinkRegex = /((forward|>>|next|次[のへ]|→)+)/i
settings.prevLinkRegex = /((back|<<|prev(ious)?|前[のへ]|←)+)/i
settings.newTabPosition = 'right'
settings.omnibarMaxResults = 12
settings.historyMUOrder = false
settings.theme = `
#sk_status, #sk_find {
  font-size: 12pt;
}
#sk_omnibarSearchResult .highlight {
  background: #f9ec89;
}
`

// Key mappings
api.map('h', 'E') // previousTab
api.map('l', 'R') // nextTab
api.map('F', 'C') // Open a link in non-active new tab
api.map('H', 'S') // Go back in history
api.map('L', 'D') // Go forward in history

// Key mappings on insert mode
api.iunmap("<Ctrl-e>") // disable move the cursor to the end of the line.
